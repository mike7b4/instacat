# coding: utf8
# try something like
@auth.requires_login()
def index():
    return dict(form=SQLFORM.grid(db.albums, fields=[db.albums.modified, db.albums.name, db.albums.groups],search_widget=None, create=True, deletable=False, editable=True))



@service.jsonrpc
def add():
    if request.vars.has_key("name")==False or request.vars.name=="":
        return response.json({"result" : "denied"})
        
    id = db.albums.insert(name=request.vars.name.capitalize())
    item = db(db.albums.id == id).select()[0]
    return response.json({"result" : "ok", "uuid" : item.uuid, "name" : item.name})

@service.jsonrpc
def list():
    rows=db(db.albums).select()
    return response.json({ "result" : "ok" , "albums" : [(album.name,album.uuid) for album in rows]})
    
def call():
    session.forget()
    return service()
