# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a samples controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - call exposes all registered services (none by default)
#########################################################################


@auth.requires_login()
def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simple replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Welcome to instamike!")
    return dict(message=T(""))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())

@auth.requires_login()
def create_api_key():
    if request.vars.has_key("pub_key") == False:
        return response.json({"result" : "error", "message" : "denied"})

    if len(db(db.api_keys.user_uuid==request.vars.pub_key).select(db.api_keys.ALL))==0:
        db.api_keys.insert(user_uuid = auth_user.uuid, api_key = request.vars.api_key)
        
    return response.json({"result" : "error", "message" : "denied", "session" : session.session})
