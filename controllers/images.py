# coding: utf8
# try something like
from fnmatch import fnmatch

@auth.requires_login()
def index():
    # first check if its a POST if not a POST but already session.album_uuid use that as default else set to None
    album_uuid = request.vars.album_uuid or session.album_uuid or ""
    search_form = SQLFORM.factory(Field('album_uuid', 'string', length=64, requires=IS_IN_DB(db, db.albums.uuid, '%(name)s', zero=None,  orderby=db.albums.name), default=album_uuid, label=T('')))
    search_form.element(_id='no_table_album_uuid')['_onchange']="form.submit();"
    submit = search_form.element('input',_type='submit') 
    submit["_style"] = 'display: none;'
    querys=[]
    if len(request.args):
        title = db(db.albums.uuid==request.args(0)).select(db.albums.name)[0].name
        session.album_uuid = request.vars.album_uuid
    elif request.vars.has_key("album_uuid") and request.vars.album_uuid!="":
        album_uuid = session.album_uuid = request.vars.album_uuid
    elif album_uuid=="":
        title = "All"
        images=db().select(db.images.ALL)
        return dict(images = images, title=title, form=search_form)
    
    try:
        title = db(db.albums.uuid==session.album_uuid).select(db.albums.name)[0].name
    except:
        title = session.album_uuid+"?"
        images=db().select(db.images.ALL)
        session.album_uuid=""
        return dict(images = images, title=title, form=search_form)
   
    images=db(db.images.album_uuid==session.album_uuid).select(db.images.ALL)
    return dict(images = images, title=title, form=search_form)

@auth.requires_login()
def new():
    filename=""
    form = SQLFORM(db.images)
    if form.process(onvalidation=upload_validate).accepted:
        filename=form.vars.filename
        image_make_small(form.vars.filename)
        image_make_medium(form.vars.filename)
        #redirect("image/"+form.vars.uuid)
    
    return dict(form=form, title="FIXME", filename=filename)

#@auth.requires_login()
def add():
    if request.vars.has_key("title")==False or request.vars.has_key("body")==False or request.vars.has_key("album_uuid")==False or request.vars.has_key("filename")==False:
        return response.json({"result" : "denied", "post" : str(request.vars)})
    if len(db(db.albums.uuid == request.vars.album_uuid).select()) != 1:
        return response.json({"result" : "error", "message" : "Album don't exist"})

    image = db.images.filename.store(request.vars.filename.file, request.vars.filename.filename.lower().replace(" ", "_"))
    id = db.images.insert(title=request.vars.title.capitalize(), body=request.vars.body, album_uuid=request.vars.album_uuid, filename=image)
    item = db(db.images.id == id).select()[0]
    image_make_small(item.filename)
    image_make_medium(item.filename)
    return response.json({"result" : "ok", "uuid" : item.uuid, "title" : item.title, "filename" : item.filename})


# fixme only show if public or logged in user
def image():
    try:
        image=db(db.images.uuid==request.args(0)).select(db.images.ALL)
    except:
        redirect("index")
        
    return dict(image=image)

def ajax_image_from_uuid():
    try:
        image=db(db.images.uuid==request.args(0)).select(db.images.ALL)[0]
        return dict(body=IMG(_src="download/"+image.filename, _alt=image.title), button=A("Full", _href=URL("instamike","images/full",request.args(0))))
    except:
        return "Denied"

def full():
    try:
        image=db(db.images.uuid==request.args(0)).select(db.images.ALL)[0]
        return dict(title=image.title, filename=image.filename, uuid=request.args(0))
    except:
        return dict(title="denied", filename="", body="Denied")


def small():
    try:
        images=[( image.title, image.filename.replace(".jpg", "_small.jpg").replace(".png", "_medium.png"), image.uuid) for image in db().select(db.images.ALL)]
        return dict(title=image.title, filename=image.filename.replace(".jpg", "_small.jpg"), uuid=request.args(0))
    except:
        return dict(title="denied", body="Denied")


def original():
    try:
        image=db(db.images.uuid==request.args(0)).select(db.images.ALL)[0]
        return dict(title=image.title, filename=image.filename)
    except:
        redirect("index")

# FIXME only if allowed user
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


# FIXME only list if public and user_uuid specified
def list():
    if len(request.args) == 0:
        images=[(image.title, image.filename) for image in db().select(db.images.ALL)]
    else:
        images=[(image.title, image.filename) for image in db(db.images.album_uuid==request.args(0)).select(db.images.ALL)]

    return response.json({ "success" : "ok", "images" : images})



# FIXME only list if public and user_uuid specified
def list_small():
    if len(request.args) == 0:
        images=[( image.title, image.filename.replace(".jpg", "_small.jpg").replace(".png", "_small.png"), image.uuid) for image in db().select(db.images.ALL)]
    else:
        images=[( image.title, image.filename.replace(".jpg", "_small.jpg").replace(".png", "_small.png"), image.uuid) for image in db(db.images.album_uuid==request.args(0)).select(db.images.ALL)]    
    
    return response.json({ "success" : "ok", "images" : images})
