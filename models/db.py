# -*- coding: utf-8 -*-

#########################################################################
## This scaffolding model makes your app work on Google App Engine too
## File is released under public domain and you can use without limitations
#########################################################################

## if SSL/HTTPS is properly configured and you want all HTTP requests to
## be redirected to HTTPS, uncomment the line below:
# request.requires_https()
import uuid
import datetime
db = DAL('sqlite://storage.sqlite',pool_size=1,check_reserved=['all'])

## by default give a view/generic.extension to all actions from localhost
## none otherwise. a pattern can be 'controller/function.extension'
response.generic_patterns = ['*'] if request.is_local else []
## (optional) optimize handling of static files
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

#########################################################################
## Here is sample code if you need for
## - email capabilities
## - authentication (registration, login, logout, ... )
## - authorization (role based authorization)
## - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
## - old style crud actions
## (more options discussed in gluon/tools.py)
#########################################################################

from gluon.tools import Auth, Crud, Service, PluginManager, prettydate
auth = Auth(db)
auth.settings.allow_basic_login = True
auth.settings.extra_fields['auth_user']=[Field("uuid", "string", length = 64, readable=False, writable=False, default=lambda: uuid.uuid4() )]
crud, service, plugins = Crud(db), Service(), PluginManager()

## create all tables needed by auth if not custom tables
auth.define_tables(username=False, signature=False)
auth.settings.actions_disabled.append('register')
## configure email
mail = auth.settings.mailer
mail.settings.server = 'logging' or 'smtp.gmail.com:587'
mail.settings.sender = 'admin@localhost'
mail.settings.login = 'username:password'

## configure auth policy

auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = True
auth.settings.reset_password_requires_verification = True

## if you need to use OpenID, Facebook, MySpace, Twitter, Linkedin, etc.
## register with janrain.com, write your domain:api_key in private/janrain.key
from gluon.contrib.login_methods.rpx_account import use_janrain
use_janrain(auth, filename='private/janrain.key')

#########################################################################
## Define your tables below (or better in another model file) for example
##
## >>> db.define_table('mytable',Field('myfield','string'))
##
## Fields can be 'string','text','password','integer','double','boolean'
##       'date','time','datetime','blob','upload', 'reference TABLENAME'
## There is an implicit 'id integer autoincrement' field
## Consult manual for more options, validators, etc.
##
## More API examples for controllers:
##
## >>> db.mytable.insert(myfield='value')
## >>> rows=db(db.mytable.myfield=='value').select(db.mytable.ALL)
## >>> for row in rows: print row.id, row.myfield
#########################################################################

## after defining tables, uncomment below to enable auditing
# auth.enable_record_versioning(db)

db.define_table('groups',
                Field('uuid', readable=False, writable=False, default = lambda: uuid.uuid4()),
                Field('created', 'datetime', writable=False, default=datetime.datetime.utcnow()),
                Field('modified', 'datetime', writable=False, default=datetime.datetime.utcnow()),
                Field("user_uuid", 'string', requires=IS_IN_DB(db, db.auth_user.uuid)),
                Field('name', 'string', requires=IS_NOT_EMPTY())
                )


db.define_table('albums',
                Field('uuid', readable=False, writable=False, default = lambda: uuid.uuid4()),
                Field('created', 'datetime', writable=False, default=datetime.datetime.utcnow()),
                Field('modified', 'datetime', writable=False, default=datetime.datetime.utcnow()),                
                Field("user_uuid", 'string', requires=IS_IN_DB(db, db.auth_user.uuid), writable=False),
                Field('name', 'string', unique=True, requires=IS_NOT_EMPTY()),
                Field('groups', 'string'))




db.define_table('images',
                Field('uuid', readable=False, writable=False, default = lambda: uuid.uuid4()),
                Field('created', 'datetime', writable=False, default=datetime.datetime.utcnow()),
                Field('modified', 'datetime', writable=False, readable=False, default=datetime.datetime.utcnow()),
                Field('album_uuid', 'string', length=64, requires=IS_IN_DB(db, db.albums.uuid, '%(name)s', zero=None,  orderby=db.albums.name), label=T('Album')),
                Field('filename', 'upload', requires=IS_IMAGE(extensions=('jpeg', 'png','jpg'))),
                Field('title', 'string', length=72),
                Field('body', 'text', length=1024)
                )
                

db.define_table('comments',
                Field('uuid', readable=False, writable=False, default = lambda: uuid.uuid4()),
                Field('created', 'datetime', writable=False, default=datetime.datetime.utcnow()),
                Field('modified', 'datetime', writable=False, default=datetime.datetime.utcnow()),                
                Field('image_uuid', 'string', length=64, readable=False, writable=False, requires=IS_IN_DB(db, db.images.uuid))
                )

db.define_table('api_keys',
                Field('uuid', readable=False, writable=False, default = lambda: uuid.uuid4()),
                Field('created', 'datetime', writable=False, default=datetime.datetime.utcnow()),
                Field('modified', 'datetime', writable=False, default=datetime.datetime.utcnow()),                
                Field('user_uuid', 'string', length=64, readable=False, writable=False, requires=IS_IN_DB(db, db.auth_user.uuid)),
                Field('api_key','string', length = 32)
                )



def upload_validate(form):
    filename=form.vars.filename.filename.lower()
    form.vars.filename.filename=filename
