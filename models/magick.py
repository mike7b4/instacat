# coding: utf8
import os
from fnmatch import fnmatch
import PythonMagick as Magick


def image_make_small(filename):
    filename=request.folder+os.path.sep+"uploads"+os.path.sep+filename
    if fnmatch(filename, "*.jpg") or fnmatch(filename, "*.png"):  
        outfile=filename.replace(".jpg","_small.jpg").replace(".png", "_small.png")
        img = Magick.Image(filename)
        img.transform("320x200")
        img.write(outfile)


def image_make_medium(filename):
    filename=request.folder+os.path.sep+"uploads"+os.path.sep+filename
    if fnmatch(filename, "*.jpg") or fnmatch(filename, "*.png"):  
        outfile=filename.replace(".jpg","_medium.jpg").replace(".png", "_medium.png")
        img = Magick.Image(filename)
        #if img.height()>
        img.transform("2048x1536")
        img.write(outfile)
